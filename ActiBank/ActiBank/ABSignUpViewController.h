//
//  ABSignUpViewController.h
//  ActivateDemo
//
//  Created by Zack Ulrich on 1/8/14.
//  Copyright (c) 2014 Zack Ulrich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABSignUpViewController : UIViewController
- (IBAction)cancelButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *signUpImageView;
@property (weak, nonatomic) IBOutlet UITextView *signUpTextDetailsView;
- (IBAction)startTrialButton:(id)sender;

@end
