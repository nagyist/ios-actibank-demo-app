//
//  ABSignUpViewController.m
//  ActivateDemo
//
//  Created by Zack Ulrich on 1/8/14.
//  Copyright (c) 2014 Zack Ulrich. All rights reserved.
//

#import "ABSignUpViewController.h"
#import "SVProgressHUD.h"

@interface ABSignUpViewController ()

@end

@implementation ABSignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)startTrialButton:(id)sender {
    
    [SVProgressHUD showWithStatus:@"Sending" maskType:SVProgressHUDMaskTypeGradient];
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(signedUp) userInfo:nil repeats:NO];
}

-(void)signedUp
{
    [SVProgressHUD showSuccessWithStatus:@"Enrolled"];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
