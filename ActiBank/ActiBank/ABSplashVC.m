//
//  ABSplashVC.m
//  ActiBank
//
//  Created by Zack Ulrich on 1/16/14.
//  Copyright (c) 2014 Zack Ulrich. All rights reserved.
//

#import "ABSplashVC.h"

@interface ABSplashVC ()

@end

@implementation ABSplashVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startup) name:@"Start_Notification" object:nil];
    
   
}

-(void)startup
{
    [self performSegueWithIdentifier:@"startSegue" sender:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
