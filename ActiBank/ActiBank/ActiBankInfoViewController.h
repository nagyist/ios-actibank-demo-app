//
//  BankInfoViewController.h
//  ActivateDemo
//
//  Created by Zack Ulrich on 12/5/13.
//  Copyright (c) 2013 Zack Ulrich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActiBankInfoViewController : UIViewController

@property (nonatomic, strong) NSString *selectedType;
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;

@end
