//
//  BankInfoViewController.m
//  ActivateDemo
//
//  Created by Zack Ulrich on 12/5/13.
//  Copyright (c) 2013 Zack Ulrich. All rights reserved.
//

#import "ActiBankInfoViewController.h"

@interface ActiBankInfoViewController ()
{
    NSString *lendingText;
    NSString *depositsText;
    NSString *aboutUsText;
}

@end

@implementation ActiBankInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupText];
	// Do any additional setup after loading the view.
}

-(void)setupText
{
    lendingText = @"At ActiBank, we know your financial goals because we take the time to get to know you. Whether you’re buying a home, adding on to your house to make room for another family member, or just looking to use your home’s equity to have money when you need it, we’re here to help. With our extensive personal loan and credit services, we can help you understand your options so that you can make the right financial decisions. \n Installment loans are an excellent solution for many borrowing needs. With an installment loan, you repay the loan over a specific period of time (term) with set monthly payments. Whether you are in the market for a new or used car or you are seeking to finance a recreational vehicle, motorcycle, or boat, an installment loan is an easy way to get the money you need. \n If you own your own home and need some cash, a home equity loan may be one of the best ways to go. A home equity loan is a great way to borrow for purchasing a car or a home renovation. With a home equity loan you have the flexibility to utilize the funds for just about anything you need. Your interest may even be tax deductible.* \n Whether you are just beginning to look at homes, planning to build your dream home, or want to refinance your existing home, ActiBank has the mortgage to fit your needs and make your dreams come true.";
    
    depositsText = @"Open a checking account with ActiBank and say hello to convenience banking. Our checking accounts are designed to meet your everyday needs and provide easy access to your money. Review the checking account options we offer and decide which one is best for you.";
    
    aboutUsText = @"Something here";
    
    
    if ([self.selectedType isEqualToString:@"lending"])
    {
        self.infoTextView.text = lendingText;
    }
    
    else if([self.selectedType isEqualToString:@"deposits"])
    {
        self.infoTextView.text = depositsText;
    }
    
    else if([self.selectedType isEqualToString:@"aboutUs"])
    {
        self.infoTextView.text = aboutUsText;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
