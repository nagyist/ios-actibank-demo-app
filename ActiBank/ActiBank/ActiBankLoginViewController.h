//
//  ViewController.h
//  ActivateDemo
//
//  Created by Zack Ulrich on 12/5/13.
//  Copyright (c) 2013 Zack Ulrich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActiBankLoginViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UIView *passwordTextField;


- (IBAction)loginPressed:(id)sender;

@end
