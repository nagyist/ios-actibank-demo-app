//
//  ViewController.m
//  ActivateDemo
//
//  Created by Zack Ulrich on 12/5/13.
//  Copyright (c) 2013 Zack Ulrich. All rights reserved.
//

#import "ActiBankLoginViewController.h"

@interface ActiBankLoginViewController ()
{
}

@end

@implementation ActiBankLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self presentActivateAd];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)loginPressed:(id)sender {
    
    [self performSegueWithIdentifier:@"loginActiBankSegue" sender:self];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}
@end
