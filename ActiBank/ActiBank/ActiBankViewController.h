//
//  ActiBankViewController.h
//  ActivateDemo
//
//  Created by Zack Ulrich on 12/5/13.
//  Copyright (c) 2013 Zack Ulrich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActiBankViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSURLConnectionDelegate, NSURLConnectionDataDelegate, NSXMLParserDelegate>

@property (weak, nonatomic) IBOutlet UITableView *infoTable;

@end
