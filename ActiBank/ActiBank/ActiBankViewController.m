//
//  ActiBankViewController.m
//  ActivateDemo
//
//  Created by Zack Ulrich on 12/5/13.
//  Copyright (c) 2013 Zack Ulrich. All rights reserved.
//

#import "ActiBankViewController.h"
#import "ActiBankInfoViewController.h"

const int Y_TRANSFORM_AD_BANK = 70;
const int Y_TRANSFORM_TABLE = 100;

@interface ActiBankViewController ()
{
    UIView *actiBankAdView;
    
   
    
}



@end

@implementation ActiBankViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    actiBankAdView = [UIView new];
    //[self presentActivateAd];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"view will appear");
}

-(void)viewDidAppear:(BOOL)animated
{
    //NSLog(@"view did appear");
    //[self nothing];
}

-(void)nothing
{
    
}



-(void)presentActivateAd
{
    
    NSLog(@"presenting activate ad");
    
    UIImageView *closeImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    closeImage.image = [UIImage imageNamed:@"x.png"];
    closeImage.userInteractionEnabled = YES;
    
    UIImageView *adImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 290, 90)];
    
    adImage.image = [UIImage imageNamed:@"Mobile_REWARDS_imageV3.png"];
    //adImage.backgroundColor = [UIColor blueColor];
    adImage.userInteractionEnabled = YES;
    
    [closeImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissView)]];
    
    
    [adImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentSignUpScreen)]];
    
    [actiBankAdView addSubview:adImage];
    [actiBankAdView addSubview:closeImage];
    
    [self.view addSubview:actiBankAdView];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    actiBankAdView.frame = CGRectMake(10, Y_TRANSFORM_AD_BANK, 300, 90);
    
    self.infoTable.frame = CGRectMake(self.infoTable.frame.origin.x, self.infoTable.frame.origin.y + Y_TRANSFORM_TABLE, self.infoTable.frame.size.width, self.infoTable.frame.size.height - Y_TRANSFORM_TABLE);
    
    
    [UIView commitAnimations];
}

-(void)presentSignUpScreen
{
    NSLog(@"present sign up");
    
    [self performSegueWithIdentifier:@"bankToSignUp" sender:self];
    [self dismissView];
}

-(void)dismissView
{
    
    NSLog(@"dismiss view called");
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    actiBankAdView.frame = CGRectMake(actiBankAdView.frame.origin.x, -300, actiBankAdView.frame.size.width, actiBankAdView.frame.size.height);
    
    self.infoTable.frame = CGRectMake(self.infoTable.frame.origin.x, self.infoTable.frame.origin.y - Y_TRANSFORM_TABLE, self.infoTable.frame.size.width, self.infoTable.frame.size.height + Y_TRANSFORM_TABLE);
    
    [UIView commitAnimations];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"bankInfoSegue" sender:indexPath];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
 -(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"actiBankCell"];
    
    
    if (indexPath.row == 0)
    {
        cell.textLabel.text = @"Deposits";
    }
    
    else if(indexPath.row == 1)
    {
        cell.textLabel.text = @"Lending";
    }
    else if(indexPath.row == 2)
    {
        cell.textLabel.text = @"About Us";
    }
    
    
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
        
    if ([segue.identifier isEqualToString:@"bankInfoSegue"]) {
        
        ActiBankInfoViewController *vc = (ActiBankInfoViewController *)segue.destinationViewController;
        
        
        NSIndexPath *ip = (NSIndexPath *)sender;
        
        if (ip.row == 0)
        {
            vc.selectedType = @"lending";
        }
        else if(ip.row == 1)
        {
            vc.selectedType = @"deposits";
        }
        
        else if(ip.row == 2)
        {
            vc.selectedType = @"aboutUs";
        }

    }
    else if([segue.identifier isEqualToString:@"bankToSignUp"])
    {
        
    }
    
    
}


@end
