//
//  AppDelegate.h
//  ActiBank
//
//  Created by Zack Ulrich on 1/13/14.
//  Copyright (c) 2014 Zack Ulrich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
