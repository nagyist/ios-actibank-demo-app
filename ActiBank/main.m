//
//  main.m
//  ActiBank
//
//  Created by Zack Ulrich on 1/13/14.
//  Copyright (c) 2014 Zack Ulrich. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "libEnsighten.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        
        EnsightenSetLoggingLevel(LoggingLevelVerbose);
        EnsightenBootstrap();
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
