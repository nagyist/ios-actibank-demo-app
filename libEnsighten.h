//
//  libEnsighten.h
//  Copyright 2011 Ensighten, LLC. All rights reserved.


@interface Ensighten : NSObject


// Initial call to launch Ensighten
void EnsightenBootstrap ();

// For overriding default values and plist keys
// Consult Ensighten before using this method
void EnsightenBootstrapWithOverrides (NSString* clientID, NSString* appID, NSString* version_plistKey);


typedef enum EnsightenLoggingLevel {
    
    LoggingLevelBasic =   0,
    LoggingLevelVerbose = 1
    
} EnsightenLoggingLevel;
 


void EnsightenSetLoggingLevel (EnsightenLoggingLevel level);
// For manually notifiying of screen appearances.
// ** Rare case **

+ (void)viewDidAppear:(NSString*)viewControllerName;



@end

